<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get("/testS", 'TestController@testSimulation');
Route::get("/testExcel","TestController@testExcel");
Route::get('/', function () {
    return redirect('login');
});

Auth::routes();
Route::group(['middleware' => ['auth:web']], function () {
    Route::get('/sync/tech-routing', 'TechRoutingController@syncTechRouting')->name('syncTechRouting');
    Route::get('/work-center-data', 'WorkCenterController@getCenterData')->name('getCenterData');
    Route::resource('resource', 'ResourceSetController');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::post('/rest-time/{id}', 'RestTimeController@createData');
    Route::resource('work-type', 'WorkTypeController')->except('show');
    Route::resource('rest-time', 'RestTimeController')->except('show');
    Route::get('/search-bom', 'BomController@index')->name('search-bom');
    Route::get('/result-bom', 'BomController@resultBom')->name('result-bom');
    Route::get('/list-bom/{data}', 'BomController@listBom')->name('list-bom');
    Route::get('/sale-order', 'SaleOrderController@index')->name('sale-order');
    Route::get('/work-center', 'WorkCenterController@index')->name('work-center');
    // Route::get('/work-center-data', 'WorkCenterController@getCenterData')->name('getCenterData');
    Route::get('/load-schedule', 'LoadScheduleController@index')->name('load-schedule');
    Route::get('/full-calendar', 'CalendarController@fullcalendar')->name('full-calendar');
    Route::get('/year-calendar', 'CalendarController@yearcalendar')->name('year-calendar');
    Route::post('/compare-data', 'LoadScheduleController@importFile')->name('compare-data');
    Route::get('/work-center/{id}/show', 'WorkCenterController@show')->name('work-center-show');
    Route::get('/sale-order-result', 'SaleOrderController@orderResult')->name('sale-order-result');
    Route::put('/rest-time/{id}/setup/{setup_id}', 'RestTimeController@updateData')->name('update-data');
    Route::get('/process-calendar', 'ProcessCalendarController@processcalendar')->name('process-calendar');
    Route::delete('/rest-time/{id}/setup/{setup_id}', 'RestTimeController@deleteData')->name('delete-data');
    Route::get('/load-schedule-result', 'LoadScheduleController@scheduleResult')->name('load-schedule-result');
    Route::get('/sale-order-result-form', 'SaleOrderController@synchroizedForm')->name('sale-order-result-form');
    Route::get('/load-manufacture-result','LoadScheduleController@manufacture')->name('load-manufacture-result');
    Route::get('/adjust-process-calendar', 'ProcessCalendarController@showProcessCalendar')->name('show-process-calendar');
    ROUTE::get('/search-loaded-manufacture','LoadManufactureController@index')->name('search-manufacture');

    Route::resource('management', 'ManageMentController');
    Route::resource('role-set', 'RoleSetController');

    Route::group(['prefix' => 'tech-routing'], function () {
// Route::get('/sync/tech-routing', 'TechRoutingController@syncTechRouting')->name('syncTechRouting');
        Route::get('/', 'TechRoutingController@index')->name('tech-routing.index');
        Route::get('/edit/{id}', 'TechRoutingController@edit');
        Route::put('/{id}', 'TechRoutingController@update')->name('tech-routing.update');
    });

    Route::group(['prefix' => 'simulation'], function () {
        Route::get('source-order', 'SimulationSchemeController@sourceOrder')->name('source-order');
        Route::get('get-source-order', 'SimulationSchemeController@getSourceOrderData')->name('get-source-order');
        Route::get('source-order-result', 'SimulationSchemeController@sourceOrderResult')->name('source-order-result');
        Route::get('get-manufacture-order', 'SimulationSchemeController@getManufactureOrderData')->name('get-manufacture-order');
        Route::get('manufacture-order-result', 'SimulationSchemeController@manufactureOrderResult')->name('manufacture-order-result'); //return manumacture.blade need data
        Route::get('load-source-order', 'SimulationSchemeController@getLoadSourceOrder')->name('load-source-order');
        Route::get('get-generate-scheme', 'SimulationSchemeController@getGenerateSimulation')->name('get-generate-scheme');
        Route::post('generate-simulation-scheme', 'SimulationSchemeController@generateSimulation')->name('generate-simulation-scheme');
        Route::get('search-scheme', 'SimulationSchemeController@searchScheme')->name('search-scheme');
        Route::get('search-scheme/{id}/result', 'SimulationSchemeController@searchSchemeResult');
        Route::get('search-scheme-result', 'SimulationSchemeController@getSearchSchemeResult');
        Route::post('get-search-scheme', 'SimulationSchemeController@getSearchScheme');
        Route::get('confirm-scheme','SimulationSchemeController@confirmSimulation')->name('confirm-scheme-result') ;
    });

    Route::get('/workcenter/add-default-resource', 'WorkCenterController@addDefaultResource')->name('resource-sync');

    //塗裝吊勾
    Route::resource('coating','CoatingSetController');
    Route::get('/sync','CoatingSetController@SyncToRemote')->name('sync');

    //異常原因設定
    Route::get('/abnormal-set', 'AbnormalSetController@index')->name('abnormal-set');
    Route::get('/abnormal-ctrl-add', 'AbnormalSetController@controll_page')->name('abnormal-ctrl-add');
    Route::get('/abnormal-add', 'AbnormalSetController@add')->name('abnormal-add');
    Route::get("/abnormal-del/{id}", 'AbnormalSetController@delete');
    Route::get("/abnormal-ctrl-fix/{id}", 'AbnormalSetController@controll_page');
    Route::get("/abnormal-fix/{id}", 'AbnormalSetController@edit');
    //初始模擬統計
    Route::post('/scheme-load-form','SimulationSchemeController@showLoadForm')->name('scheme-load-form');
    Route::delete('sale-order-destroy','SimulationSchemeController@destroySale')->name('sale-order.destroy');
});

