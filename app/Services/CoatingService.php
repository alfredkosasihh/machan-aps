<?php

/*撰寫人: 鄧臣宏*/
namespace App\Services;

class CoatingService {

	/*
		$E 上料區
		$H 圓蓋
		$K rpm
		$L 單位格數
		$I 一勾勾吊數量

		01 箱身
		02 配件
		03 抽屜
	*/	

	//此一時間格式運算之function，用以模仿Excel 之時間運算
    public function T($unit, $val) 
    {
        //$unit 時間單位 
        //$val  運算值

        if ($unit === 'hour') {
        	$time = 3600*$val;
        } else if ($unit === 'minute') {
            $time = 60*$val;
        } else if ($unit === 'second') {
            $time = $val;
        } else if ($unit === 'day') {
            $time = $val*3600*24;
        }
        return $time;
    }

	//換模空勾數 R
	public function getChangeDistance ($E,$H)
	{	

		if($E == "02") 
		{
			$result = 3;
		}
		else if ($H) 
		{
			$result =7;
		}
		else 
		{
			$result = 9;
		}

		return $result;

	}

	//工作預定速率 P
	public function getDefRate ($K)
	{
		return $K*0.007; 
	}

	//總長 F
	public function getPTotalLen($E)
	{
		if ($E == "01")
		{
			$result = 442.055;
		}
		else if ($E =="03")
		{
			$result = 397.355;
		}
		else {
			$result = 442.055;
		}
		return $result;
	}

	//作業TCT T
	public function getTCT ($E,$K)
	{
		$F = $this->getPTotalLen($E) ;
		$P = $this->getDefRate($K);
		return $result = (int)T('second',$F/$P*60);
	}

	//換模時間(秒) S
	public function getChangeTime ($E,$H,$K)
	{
		$R = $this->getChangeDistance($E,$H) ;
		$P = $this->getDefRate($K) ;

		return $result = _ceil(($R/$P)*60,2);

	}

	//CT時間 Q
	public function getCTTime($L,$I,$K) 
	{


		$P = $this->getDefRate($K) ;

		//四捨五入至小數點第二位
		return _ceil(((($L*0.3/$I)/$P)*60),2); 
	}

}

    function T($unit, $val) 
    {
        //$unit 時間單位 
        //$val  運算值

        if ($unit === 'hour') {
        	$time = 3600*$val;
        } else if ($unit === 'minute') {
            $time = 60*$val;
        } else if ($unit === 'second') {
            $time = $val;
        } else if ($unit === 'day') {
            $time = $val*3600*24;
        }
        return $time;
    }

    function _ceil($v , $p) {
    	$c = pow(10,$p);
    	return round($v*$c)/$c ;

    }
