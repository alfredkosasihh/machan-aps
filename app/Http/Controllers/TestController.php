<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\MachanService;
use App\Services\GetProduceService;
use App\Services\SimulationTransferService;
use Maatwebsite\Excel\Facades\Excel;
class TestController extends Controller
{
    public function __construct(MachanService $machanService, GetProduceService $proService,  SimulationTransferService $simuService)
    {
        $this->machanService = $machanService;
        $this->proService = $proService;
        $this->simuService = $simuService;
    }

    public function test() //塗裝吊鉤
    {
        $this->machanService->test();
    }

    public function testMo()
    {
        return $this->proService->testMo(request()->so_id);
    }

    public function testBom()
    {
        return $this->proService->getComBom(request()->item_id);
    }
    public function testExcel()
    {
        $result = "";$tit_r = []; $tit_l = []; $data = [];
        $tit_l[0] = "";               

        for ($i=0; $i < 26 ; $i++) { 
            $tit_r[$i] = chr(65+$i);
            $tit_l[$i+1] = chr(65+$i);
        } // init array
        $result = Excel::load('excel\aps.xlsx', function($reader) {
            $reader->takeRows(10)->toArray();
        })->get();
        for ($k=0; $k < count($result) ; $k++) {
            $L = count($result[$k]);
            for ($i=0; $i < $L/26 ; $i++) {
                if($i == floor($L/26))
                    $d = $L % 26;
                else $d = 26;
                for ($j=0; $j < $d ; $j++) { 
                    $data[$k][$tit_l[$i].$tit_r[$j]] = $result[$k][$j+$i*26];
                }
            }
        }
        dd($data);
        for ($i=0; $i < count($data); $i++) {

            $request[$i] = [
                "so_id" => $data[$i]['AA'],                         
                "mo_id" =>  $data[$i]['G'],
                "item_id" =>  $data[$i]['H'],
                "qty" => $data[$i]['AJ'],
                "aps_id" => $data[$i]['R'],
                "AL" => $data[$i]['AL'] ,
                "AN" => $data[$i]['AN'] ,
                "AK" => $data[$i]['AK'] ,

            ];
        }
        dd($request);
    }
    public function testSimulation() {

        $get = request()->only('datas');
        return $this->simuService->generateSimulation($get['datas'],true);
    }
}
