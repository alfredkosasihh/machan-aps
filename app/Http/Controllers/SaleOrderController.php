<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchSaleOrder;
use App\Repositories\SaleOrderRepository;
use App\Services\GetProduceService;

class SaleOrderController extends Controller
{
    protected $saleRepo;
    protected $produceService;

    public function __construct(GetProduceService $produceService, SaleOrderRepository $saleRepo)
    {
        $this->saleRepo = $saleRepo;
        $this->produceService = $produceService;
    }

    public function index()
    {
        return view('dataload/sync-order', [
            'dateInfo' => $this->saleRepo->getSynchroizedDate()
        ]);
    }

    public function orderResult()
    {
        return view('dataload/sync-order-result');
    }

    public function getSaleOrderData(SearchSaleOrder $request)
    {
        $data = request([
            'org_id', 'container_date_start',
            'container_date_end', 'bill_date_start',
            'bill_date_end', 'so_id', 'customer_name',
            'amount', 'page',
        ]);

        $billStratDate = date('Ymd', strtotime($data['bill_date_start']));
        $billEndDate = date('Ymd', strtotime($data['bill_date_end']));

        $saleOrdeData = $this->produceService->getSourceOrder(
            $data['org_id'],
            $billStratDate,
            $billEndDate,
            $data['container_date_start'],
            $data['container_date_end'],
            $data['so_id'],
            $data['customer_name']
        );
        return redirect()->route('sale-order-result', [
            'org_id' => $data['org_id'],
            'bill_date_start' => $billStratDate,
            'bill_date_end' => $billEndDate,
            'container_date_start' => $data['container_date_start'],
            'container_date_end' => $data['container_date_end'],
            'so_id' => $data['so_id'],
            'customer_name' => $data['customer_name'],
        ]);
    }

    public function getCurrentLoadedData()
    {
        $data = request()->all(
            'bill_date_start',
            'bill_date_end',
            'org_id',
            'container_date_start',
            'container_date_end',
            'customer_name',
            'so_id'
        );

        return response()
            ->json($this->saleRepo->currentLoadedData($data)
            ->paginate(request()->amount));
    }

    public function synchroizedForm()
    {
        // $data = $this->saleRepo->getSynchroizedResult(request()->date);
        return view('dataload.sync-order-result-form');
    }

    public function getSynchroizedResult() //
    {
        return response()
            ->json($this->saleRepo->getSynchroizedResult(request()->date)
            ->paginate(request()->amount));
    }

    public function appSearchSo()
    {
        $data = $this->saleRepo->appSearchSo(request()->so_id);
        if (!$data) {
            return [];
        } else {
            return $data;
        }
    }

    public function appSearchCustomer()
    {
        $data = $this->saleRepo->appSearchCustomer(request()->customer_name);
        if (!$data) {
            return [];
        } else {
            return $data;
        }
    }
}

// $querybuilder = SaleOrder::where('org_id', $data['org_id'])
        //     ->whereBetween('bill_date', [$data['bill_date_start'], $data['bill_date_end']])
        //     ->where('so_id', 'like', '%'.$data['so_id'].'%')
        //     ->where('customer_name', 'like', '%'.$data['customer_name'].'%');

        // if ($data['container_date_start'] && $data['container_date_end']) {
        //     $querybuilder->whereBetween('container_date', [$data['container_date_start'], $data['container_date_end']]);
        // } elseif ($data['container_date_start'] && !$data['container_date_end']) {
        //     $querybuilder->whereDate('container_date', $data['container_date_start']);
        // }  elseif (!$data['container_date_start'] && $data['container_date_end']) {
        //     $querybuilder->whereDate('container_date', $data['container_date_end']);
        // }
        // return response()->json($saleOrdeData->paginate(request()->amount));]
