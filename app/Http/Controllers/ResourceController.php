<?php

namespace App\Http\Controllers;

use App\Repositories\ResourceRepository;
use App\Entities\Organization;

class ResourceController extends Controller
{
    protected $resRepo;

    public function __construct(ResourceRepository $resRepo)
    {
        $this->resRepo = $resRepo;
    }

    public function getOrganization()
    {
        return Organization::get();
    }

    public function getResource()
    {
        $data = request()->value;
        $result = $this->resRepo->index($data);
        return response()->json($result);
    }
}
