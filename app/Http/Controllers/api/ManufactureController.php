<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\Manufacture;
use App\Entities\SaleOrder;
use App\Entities\ChildPart;
use App\Entities\ParentPart;
use App\Repositories\ManufactureRepository;
use App\Services\MachanService;

class ManufactureController extends Controller
{

    public function __construct(ManufactureRepository $manuRepo, MachanService $machanService)
    {
        $this->manuRepo = $manuRepo;
        $this->machanService = $machanService;
    }

    public function getManufacture()
    {
        $data = request()->all(['online_date', 'manufacture', 'customer', 'sale_order']);
        if ($data['online_date'] == null && $data['manufacture'] == null && $data['customer'] == null && $data['sale_order'] == null) {
            return [];
        }
        $result = Manufacture::when($data['manufacture'], function ($query, $manufacture) {
            $query->where('mo_id', 'like', '%'.$manufacture.'%');
        })
        ->when($data['customer'], function ($query, $customer) {
            $query->where('customer', 'like', '%'.$customer.'%');
        })
        ->when($data['online_date'], function ($query, $date) {
            $query->whereDate('complete_date', $date);
        })
        ->when($data['sale_order'], function ($query, $saleOrder) {
            $query->where('so_id', 'like', '%'.$saleOrder.'%');
        });
        $result->with('relatedTechRoute')->with('relatedParentPart.downstreamChild.parent');
        return response()->json($result->get());
    }

    public function getNextPart()
    {
        $data = request(['sale_order', 'id']);
        $parentPart = SaleOrder::where('so_id', $data['sale_order'])->first()->item;
        $result = ChildPart::where('item_id', $parentPart)
            ->where('down_id', $data['id'])
            ->with(['relatedTopParent.manufactures' => function ($query) use ($data) {
                $query->where('so_id', $data['sale_order'])->with('relatedTechRoute');
            }])
            ->get();
        return response()->json($result);
    }

    public function getCurrentSatge()
    {
        $data = request(['sale_order', 'item']);
        $result = Manufacture::where('so_id', $data['sale_order'])
            ->where('item_id', $data['item'])
            ->with('relatedTechRoute')
            ->with('relatedParentPart.downstreamChild.parent')
            ->first();
        return response()->json($result);
    }

    public function getPrevMo()
    {
        $data = request(['so_id', 'item_id']);
        $manufactures = Manufacture::where('so_id', $data['so_id'])
            ->where('item_id', $data['item_id'])
            ->with('relatedTechRoute')
            ->get();
        return response()->json($manufactures);
    }

    public function appSearchMo()
    {
        $data = $this->manuRepo->appSearchMo(request()->mo_id);
        if (!$data) {
            return [];
        } else {
            return $data;
        }
    }

    public function getCurrentStageCom()
    {
        $data = request(['item_id']);
        $parent = ParentPart::where('material_id', $data)->first();
        $child = ChildPart::where('top_id', $parent->id)->with('parent')
                    ->whereHas('parent', function ($query) {
                        $query->where('fetch_type', '<>', '0');
                    })
                    ->get();
        return response()->json($child);
    }

    public function getNowManufacture()
    {
        $data = request()->all(['online_date', 'manufacture', 'customer', 'sale_order', 'routing_level', 'org_id']);
        if ($data['online_date'] == null && $data['manufacture'] == null && $data['customer'] == null && $data['sale_order'] == null && $data['routing_level'] == null) {
            return [];
        }
        $result = Manufacture::when($data['manufacture'], function ($query, $manufacture) {
                $query->where('mo_id', 'like', '%'.$manufacture.'%');
        })
        ->when($data['routing_level'], function ($query, $routing_level) {
            $query->whereHas('relatedTechRoute', function ($query) use ($routing_level) {
                $query->where('routing_level', $routing_level);
            });
        })
        ->whereHas('relatedTechRoute', function ($query) use ($data) {
            $query->where('factory_id', $data['org_id']);
        })
        ->when($data['customer'], function ($query, $customer) {
            $query->where('customer', 'like', '%'.$customer.'%');
        })
        ->when($data['online_date'], function ($query, $date) {
            $query->whereDate('complete_date', $date);
        })
        ->when($data['sale_order'], function ($query, $saleOrder) {
            $query->where('so_id', 'like', '%'.$saleOrder.'%');
        })
        ->with('relatedTechRoute')->with('relatedParentPart.downstreamChild.parent')
        ->get()
        ->sortByDesc(function($query){
             return $query->relatedTechRoute->routing_level;
        });
        return response()->json($result->values());
    }

    public function getSubMaterialInfoV() // 製令子件資訊 API
    {
        if (request()->mo_id) {
            $title = Manufacture::where('mo_id', 'like', '%'.request()->mo_id.'%')
            ->with('relatedTechRoute')->with('relatedParentPart')
            ->get();
            $title = json_decode(json_encode($title));
            
            $result = $this->machanService->getSubMaterialInfoV(request()->mo_id);
            $result = json_decode($result);
            
            $title[0]->related_parent_part->downstream_child = $result;
            return $title;
        } else {
            return 'no mo_id';
        }
    }
}
