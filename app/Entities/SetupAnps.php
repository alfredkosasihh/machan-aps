<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class SetupAnps extends Model
{
    protected $fillable = [
        'bill_no',
        'cu_material_id',
        'cu_org_id',
        'cu_tht',
        'cu_target_prod_qty',
        'cu_target_person_qty',
        'cu_ht',
        'cu_rw_tht',
        'cu_std_ct',
        'cu_adj_tht',
        'cu_adj_ht',
        'cu_packing_tht',
        'cu_adj_packing_tht',
        'cu_wp',
        'cu_db_rate'
    ];
}
