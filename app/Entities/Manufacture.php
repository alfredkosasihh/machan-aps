<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Manufacture extends Model
{
    protected $fillable = [
        'mo_id',
        'item_id',
        'item_name',
        'qty',
        'techroutekey_id',
        'online_date',
        'so_id',
        'batch',
        'complete_date',
        'customer',
        'demand_complete_date',
        'bill_date',
        'current_state',
    ];

    public function relatedParentPart()
    {
        return $this->belongsTo('App\Entities\ParentPart', 'item_id', 'material_id');
    }

    public function relatedTechRoute()
    {
        return $this->belongsTo('App\Entities\TechRouting', 'techroutekey_id', 'tech_routing_id');
    }
}
