<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class ChildPart extends Model
{
    protected $fillable = ['row_no', 'row_id', 'unit_id', 'remark', 'fetch_type', 'top_id', 'down_id', 'unit_qty', 'nuse_qty', 'base_qty', 'item_id', 'level'];

    public function parent()
    {
        return $this->belongsTo('App\Entities\ParentPart', 'down_id');
    }

    public function relatedTopParent()
    {
        return $this->belongsTo('App\Entities\ParentPart', 'top_id');
    }
}
