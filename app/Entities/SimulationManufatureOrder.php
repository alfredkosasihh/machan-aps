<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class SimulationManufatureOrder extends Model
{
    protected $fillable = [
        'mo_id', 'item_id', 'item_name', 'qty', 'techroutekey_id', 'online_date',
        'so_id', 'status', 'complete_date', 'customer', 'customer_name'
    ];

    public function relatedParentPart()
    {
        return $this->belongsTo('App\Entities\ParentPart', 'item_id', 'material_id');
    }

    public function relatedTechRoute()
    {
        return $this->belongsTo('App\Entities\TechRouting', 'techroutekey_id', 'tech_routing_id');
    }

    public function simuSo()
    {
        return $this->belongsTo('App\Entities\SimulationSourceOrder', 'so_id', 'so_id');
    }
}
