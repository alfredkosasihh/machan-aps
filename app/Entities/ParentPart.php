<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class ParentPart extends Model
{
    protected $fillable = ['material_id', 'bomkey_id', 'bomkey_name', 'unit_id', 'techroutekey_id', 'fetch_type'];

    public function relatedChild()
    {
        return $this->hasMany('App\Entities\ChildPart', 'down_id');
    }

    public function downstreamChild()
    {
        return $this->hasMany('App\Entities\ChildPart', 'top_id');
    }

    public function manufactures()
    {
        return $this->hasMany('App\Entities\Manufacture', 'item_id', 'material_id');
    }
}
