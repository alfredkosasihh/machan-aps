<?php

namespace App\Repositories;

use App\Entities\SetupException;

class SetupExceptionRepository
{
    public function add($info) 
    { 
    	SetupException::firstOrCreate(		
    		[
    			'Type' => $info->Type,
    		 	'Exception' => $info->Exception,
                'EX_Code' => $info->EX_Code
    		]
    	);
    }

    public function fix($id,$data) 
    {
        SetupException::where('no', $id)->update($data);
    }

    public function get($id) 
    {
        return SetupException::where('no', $id)->get();
    }
    
    public function delete($id) 
    {
        SetupException::where('no', $id)->delete();
    }

    public function print() {
    	return  SetupException::get();
    }
}
