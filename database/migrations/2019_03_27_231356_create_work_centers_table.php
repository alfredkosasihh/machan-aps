<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkCentersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work_centers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('factory_id');
            $table->string('workcenter_id');
            $table->string('workcenter_name');
            $table->string('org_id');
            $table->string('transfer_factory');
            $table->string('factory_type')->nullable();
            $table->string('work_level')->nullable();
            $table->string('routing_level')->nullable();
            $table->string('aps_id')->nullable();
            $table->boolean('is_subcontract_factory');
            $table->string('subcontract_partner')->nullable();
            $table->string('person_id')->nullable();
            $table->string('off_set_ware_houseid')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('work_centers');
    }
}
