@extends('layouts.myapp')

@section('css')
<style>
    .space-item {
        margin-left: 10px;
    }
    .breadcrumb-custom {
        background-color: #3D404C;
        width: 99%;
        margin:0px auto;
        padding: 15px 15px;
        margin-bottom: 20px;
        list-style: none;
        border-radius: 4px;
        color: #fff;
    }
    .total-data {
        width: 98%;
        margin:0px auto;
    }
    .table-pos {
        margin: 0px auto;
        width: 98%;
    }
    .thead-color {
        background-color: #E85726;
        color: #fff;
        height: 10px;
    }
    .btn-secondary {
        color: #fff;
        background-color: #6c757d;
        border-color: #6c757d;
    }
    hr {
        border-top: 1px solid #ccc;
    }

    .panel-default {
        border-color: #000000;
    }
    .panel-default > .panel-heading {
        color: #fff;
        background-color: #000000;
        border-color: #000000;
    }
</style>
@endsection

@section('content')
<div id="page-wrapper">
    <div class="container-fluid">
        <h2>初始訂單明細</h2>
        <ol class="breadcrumb">
            <img src="{{ asset('img/u12.png') }}">
            <span class="space-item">排程模擬</span>
            <span class="space-item">></span>
            <span class="space-item">排程來源載入<span>
            <span class="space-item">></span>
            <span class="space-item">初始訂單明細<span>
        </ol>
        <div class="breadcrumb-custom">
            <span>資料列表</span>
        </div>
        <div class="total-data">
            載入筆數 |
            <span id="data-num"></span>
            <select name="scheme_case" id="scheme_case" style="float:right;" form='form'>
                <option value="0">標準初始模擬</option>
                <option value="1">標準模擬</option>
                <option value="0">最佳化模擬</option>
                <option value="0">標準最佳化模擬</option>
            </select>
            <label style="float:right;">選擇排單模擬計畫&emsp;</label>
        </div>
        <hr>
        <div class="total-data">
            <div style="float:right; margin-bottom:10px">
                        <button class="btn btn-success" id='new' data-toggle="modal" data-target="#add" style="display:inline">新增</button>
                    <button  class="btn btn-danger" onclick="submit_rm()">刪除</button>
            </div>
        </div>
        <form action="{{ route('get-manufacture-order')}}" method="GET" id='form'>
        <div style="margin-top:15px;">
            <table class="table table-striped table-pos" id="saleorder-data">
                <thead class="thead-color">
                    <tr>
                        <th scope="col"><input type="checkbox" name="check_all" id="check_all" onclick="checkAll(this)"></th>
                        <th scope="col">序</th>
                        <th scope="col">訂單單號</th>
                        <th scope="col">品號</th>
                        <th scope="col">客戶名稱</th>
                        <th scope="col">客戶單號</th>
                        <th scope="col">數量</th>
                        <th scope="col">結關日</th>
                        <th scope="col">排單狀態</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div style="text-align:right">
            <span style="display: inline-block; margin-top: 27px;">
                    <span>每頁顯示筆數</span>
                    <select id="amount" onchange="getSaleOrderData();$('#pagination-demo').twbsPagination('destroy');">
                        <option value="10" selected>10</option>
                        <option value="15">15</option>
                        <option value="20">20</option>
                    </select>
            </span>
            <ul id="pagination-demo" class="pagination-sm" style="vertical-align: top;"></ul>
        </div>
        <hr>
        <input type="hidden" name="status" value='0'>
        <input type="hidden" name="batch" value='1'>
    </form>
        <div style="text-align:center">
            <button id="sendBtn" class="btn btn-success btn-lg" style="width:45%"  onclick="submit()" > 確認</button>
            <a class="btn btn-secondary btn-lg" href="{{ route('source-order') }}" style="width:45%">返回</a>
        </div>
    </div>
</div>
    <div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
            <form class="form-horizontal" method="GET" action="{{ route('get-source-order') }}" id="load-sale">
                @csrf
                <input type="hidden" name="added" value="1">
                <div class="form-group">
                    <label class="col-md-2 control-label">組織</label>
                    <div class="col-md-10" id="org">
                        <select class="form-control" id="sel1" name="org_id" required>
                            <option disabled selected value="" >--- 請選擇廠別 ---</option>
                        </select>
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <label class="col-md-2 control-label">結關日期</label>
                    <div class="col-md-4">
                        <input class="form-control" type="date" id="con_date_start" name="container_date_start">
                    </div>
                    <div class="col-md-2" style="text-align:center;">
                        <label style="margin:7px;"> ~ </label>
                    </div>
                    <div class="col-md-4">
                        <input class="form-control" type="date" id="con_date_end" name="container_date_end">
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <label class="col-md-2 control-label">單據日期</label>
                    <div class="col-md-4">
                        <input class="form-control" type="date" id="bill_date_start" name="bill_date_start" required>
                    </div>
                    <div class="col-md-2" style="text-align:center;">
                        <label style="margin:7px;"> ~ </label>
                    </div>
                    <div class="col-md-4">
                        <input class="form-control" type="date" id="bill_date_end" name="bill_date_end" required>
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <label class="col-md-2 control-label">訂單單號</label>
                    <div class="col-md-10">
                        <input class="form-control" id="so_id" name="so_id">
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <label class="col-md-2 control-label">客戶名稱</label>
                    <div class="col-md-10">
                        <input class="form-control" id="customer_name" name="customer_name">
                    </div>
                    <div id="session-data" hidden>
                        {{ session('comResults') ?? '' }}
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <label class="col-md-2 control-label">訂單狀態</label>
                    <div class="col-md-10" style="font-size:12pt;">
                        <input class="inpsize" type="checkbox">&ensp;<label>未生效&emsp;</label>
                        <input class="inpsize" type="checkbox" value="0" name="status[]"><label>已生效&emsp;</label>
                        <input class="inpsize" type="checkbox" value="1" name="status[]"><label>已模擬&emsp;</label>
                        <input class="inpsize" type="checkbox" value="2" name="status[]"><label>已確認&emsp;</label>
                        <input class="inpsize" type="checkbox"><label>已排單&emsp;</label>
                        <br>
                        <input class="inpsize" type="checkbox"><label>已收回未排單&emsp;</label>
                        <input class="inpsize" type="checkbox"><label>生產中&emsp;</label>
                        <input class="inpsize" type="checkbox"><label>已出貨</label>
                    </div>

                </div>
                <hr>
                <div style="text-align:center">
                    <button type="submit" id="sendBtn" class="btn btn-success btn-lg" style="width:45%" onclick="letNoFilter()">新增</button>
                    <button type="button" class="btn btn-secondary btn-lg" style="width:45%"data-dismiss="modal">取消</button>

                </div>
            </form>
            </div>
        </div>
    </div>
        <form  action="{{route('sale-order.destroy')}}" method="POST" id="delete_stack">
            @csrf
            @method("DELETE")
            <input type="hidden" id="search" name="search">
            <div id="data_id">   
            </div>
        </form>
            <div id="data_temp">   </div>
<script  >
    let lastPage;
    
    const submit = () => {
        const e = $(':checkbox');
        if(e.filter(function(){
            return this.checked == true
        }).length) $('#form').submit();
        else alert('請至少勾取一個項目');

    }
    const checkAll = (I) => {
      const e = $(':checkbox').filter('input[name^="so_id"]');
      for (var i = e.length - 1; i >= 0; i--) {
        if(I.checked == true) e[i].checked = true ;
        else e[i].checked = false ;
        check(e[i]);
      }

    }
    //單選框事件處理,暫存處理
    const submit_rm = () =>  {
        $("#search").val(location.search);
        if($('#data_id').children().length != 0)
            $("#delete_stack").submit();
        else alert('請勾選項目');

    }
    const check = (item) => {
      if(item.checked == true){
        $('#data_temp').append($(item).clone(true).hide());
        let str = item.id.replace("arr","");
        $('#data_id').append(`<input type="hidden" value="${str}" name="id[]" />`);
      }
      else $(`#data_temp > #${item.id}`).remove();

    }
    const renderCheck = () => {
        let temp = $("#data_temp > input");
        console.log($(`#arr2`),temp);
        for ( let i = 0;   i < temp.length;i++)
            $(`#${temp[i].id}`).prop('checked',true);
        $(`#check_all`).prop('checked', false);

    }
    const getSaleOrderData = (page = 1) => {
        const amount = $('#amount').val();
        axios.get('{{ route('load-source-order') }}' + location.search, {
            params: {
                amount,
                page,
            }
        }).then(({ data }) => {
            lastPage = data.last_page;
            const orders = data.data;
            $('#data-num').text(`共 ${data.total} 筆`);
            $('#saleorder-data tbody').empty();
            console.log(data);
            orders.forEach((order, key) => {
                key = (data.current_page-1)*data.per_page+key;
                $('#saleorder-data tbody').append(`
                    <tr>
                        <td scope="row">
                            <input type="checkbox" onclick="check(this)" name="so_id[]" value="${order.so_id}" id="arr${order.id}"/>
                        </td>
                        <th scope="row">${key+1}</th>
                        <td>
                            ${order.so_id}
                        </td>
                        <td>${order.item}</td>
                        <td>${order.customer_name}</td>
                        <td>${order.customer_order}</td>
                        <td>${order.qty}</td>
                        <td>${order.container_date}</td>
                        <td>已生效</td>
                    </tr>
                `)
            });

            $('#pagination-demo').twbsPagination({
                totalPages: lastPage,
                visiblePages: 5,
                first:'頁首',
                last:'頁尾',
                prev:'<',
                next:'>',
                initiateStartPageClick: false,
                onPageClick: function (event, page) {
                    getSaleOrderData(page)
                }
            });
        }).then(function(){
            renderCheck();
        });
    }
    getSaleOrderData();

 const letNoFilter = () => {
        if(!$('input[name="status[]"]').filter(function(){
            return this.checked == true;
        }).length) {
            $('input[name="status[]"]').each(function(){
                this.checked = true;
            })
        }
        // $("#load-sale").submit();
    }
    const getOrganization = async () => {
        await axios.get('{{ route('getorganization') }}')
            .then(({ data }) => {
                data.forEach(data => {
                    $('#sel1').append(`
                        <option value="${data.factory_id}">${data.name}</option>
                    `);
                });
            });
            if ($('#session-data').text().trim() != '') {
                loadSession();
            }
    }
    getOrganization();

    const loadSession = async () => {
        const saleOrder = JSON.parse('{{ session('comResults') }}'.replace(/&quot;/g,'"'));
        const orgId = '{{ session('org_id') }}';
        let date = new Date();
        let day = date.getDate();
        let month = date.getMonth() + 1;
        let year = date.getFullYear();
        if (month < 10) month = "0" + month;
        if (day < 10) day = "0" + day;
        let today = year + "-" + month + "-" + day;
        let minusHalfMonth = date.setMonth(month - 7);
        let halfDate = new Date(minusHalfMonth);
        let halfDay = halfDate.getDate();
        let halfMonth = halfDate.getMonth() + 1;
        let halfYear = halfDate.getFullYear();
        if (halfMonth < 10) halfMonth = "0" + halfMonth;
        if (halfDay < 10) halfDay = "0" + halfDay;
        let halfToday = halfYear + "-" + halfMonth + "-" + halfDay;
        $('#sel1').val(orgId);
        $('#bill_date_start').val(halfToday);
        $('#bill_date_end').val(today);
        saleOrder.forEach((element, key) => {
            if (key < saleOrder.length - 1) {
                $("#so_id").val(function() {
                    return this.value + element + ',';
                });
            } else {
                $("#so_id").val(function() {
                    return this.value + element;
                });
            }
        });
    }
</script>
@endsection
