@extends('layouts.myapp')

@section('css')
<style>
    .space-item {
        margin-left: 10px;
    }
    .breadcrumb-custom {
        background-color: #3D404C;
        width: 99%;
        margin:0px auto;
        padding: 15px 15px;
        margin-bottom: 20px;
        list-style: none;
        border-radius: 4px;
        color: #fff;
    }
    .total-data {
        width: 98%;
        margin:0px auto;
    }
    .table-pos {
        margin: 0px auto;
        width: 98%;
    }
    .thead-color {
        background-color: #E85726;
        color: #fff;
        height: 10px;
    }
    .btn-secondary {
        color: #fff;
        background-color: #6c757d;
        border-color: #6c757d;
    }
    hr {
        border-top: 1px solid #ccc;
    }

    .panel-default {
        border-color: #000000;
    }
    .panel-default > .panel-heading {
        color: #fff;
        background-color: #000000;
        border-color: #000000;
    }
</style>
@endsection

@section('content')
<div id="page-wrapper">
    <div class="container-fluid">
        <h2>初始訂單明細</h2>
        <ol class="breadcrumb">
            <img src="{{ asset('img/u12.png') }}">
            <span class="space-item">排程模擬</span>
            <span class="space-item">></span>
            <span class="space-item">模擬方案查詢</span>
            <span class="space-item">></span>
            <span class="space-item">查詢結果</span>
            <span class="space-item">></span>
            <span class="space-item">確認模擬方案<span>
        </ol>
        <div class="breadcrumb-custom">
            <span>資料列表</span>
        </div>
        <div>
            <h3 id="scheme_id"></h3>
        </div>
        <hr>
        {{-- <form action="{{ route('generate-simulation-scheme')}}" method="GET"> --}}
        <div style="margin-top:15px;">
            <table class="table table-striped table-pos" id="generate-data">
                <thead class="thead-color">
                    <tr>
                        <th scope="col"><input type="checkbox" name="check_all" id="check_all" onclick="checkAll(this)"></th>
                        <th scope="col">製令單號</th>
                        <th scope="col">母件</th>
                        <th scope="col">來源訂單號</th>
                        <th scope="col">數量</th>
                        <th scope="col">預計出貨日</th>
                        <th scope="col">預設資源中心</th>
                        <th scope="col">APS製程碼</th>
                        <th scope="col">計畫開始</th>
                        <th scope="col">計畫結束</th>   
                        <th scope="col">建議最遲開始</th>
                        <th scope="col">建議最遲完工</th>
                        <th scope="col">建議最早開始</th>
                        <th scope="col">建議最早完工</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div style="text-align:right">
            <span style="display: inline-block; margin-top: 27px;">
                    <span>每頁顯示筆數</span>
                    <select id="amount" onchange="getGenerateScheme();$('#pagination-demo').twbsPagination('destroy');">
                        <option value="50" selected>50</option>
                        <option value="100">100</option>
                        <option value="200">200</option>
                    </select>
            </span>
            <ul id="pagination-demo" class="pagination-sm" style="vertical-align: top;"></ul>
        </div>
        <hr>
        <div style="text-align:center">
            <input type="button" id="sendBtnC" class="btn btn-success btn-lg" style="width:45%" value="確認方案" data-toggle="modal" data-target="#confirm1">
            <input type="button" id="sendBtnI" class="btn btn-success btn-lg" style="width:45%" value="發佈" data-toggle="modal" data-target="#confirm3" />
            <a class="btn btn-secondary btn-lg" href="javascript:history.back()" style="width:45%">返回</a>
        </div>
{{--     </form> --}}
    </div>
</div>
<div class="modal fade bd-example-modal-sm" id="confirm1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">

        <h3  class="text-center" style="margin:50px">是否確認方案?</h3>
        <span class ="border border-secondary">
        <button data-toggle="modal" data-target="#confirm2" class="btn btn-light" style="width:49%;font-size:20px" data-dismiss="modal" >是</button></span>
        <button  class="btn btn-light" data-dismiss="modal" style="width:49%;font-size:20px">否</button>

    </div>
  </div>
</div>
<div class="modal fade bd-example-modal-sm" id="confirm3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">

        <h3  class="text-center" style="margin:50px">確認發佈方案?</h3>
        <span class ="border border-secondary">
        <button data-toggle="modal" data-target="#confirm2" class="btn btn-light" style="width:49%;font-size:20px" data-dismiss="modal" >確定</button></span>
        <button  class="btn btn-light" data-dismiss="modal" style="width:49%;font-size:20px">取消</button>

    </div>
  </div>
</div>
<div class="modal fade bd-example-modal-sm" id="confirm2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
        <h3  class="text-center" style="margin:50px" id="success_text">方案確認成功</h3>
        <button class="btn btn-light" style="width:99%;font-size:20px" onclick="confirm()" data-dismiss="modal">確定</button>
  </div>
</div>

<script>
    let lastPage;
    $('#sendBtnI').hide();
    let issue = false;
    const checkAll = (I) => {
      const e = $(':checkbox');
      for (var i = e.length - 1; i >= 0; i--) {
        if(I.checked == true) e[i].checked = true ;
        else e[i].checked = false ;
      }
    }

    const lockForm = () => {

        const checkno = $('input[name="mo_id[]"]');
        const confirmmo = checkno.map(function(){
            let key = this.value ;
            if(this.checked){
                $(`input[name="mo_id[]"]`).filter(`input[value=${key}]`).hide();
                return Number(this.parentNode.parentNode.id);
            }
            else $(`#${key}`).hide();
        })
        return confirmmo;
    }
    
    const confirm = () => {
        $('#check_all').hide();
        const e = $(':checkbox');
        if(e.filter(function(){
            return this.checked == true
        }).length) $('#form').submit();
        else {
            alert('請至少選擇一個項目');
            return ;
            }

        let confirmmo =lockForm();
        console.log(confirmmo);
        if(!issue) {
          $('#sendBtnI').show();
          $('#sendBtnC').hide();
          $('#success_text').text('方案發佈成功');
          issue = true;
        } else  letIssue(confirmmo);
        console.log(confirmmo.toArray().toString());
        axios.post('{{route("confirm-mo")}}',{
            confirmmo : confirmmo.toArray().toString()
        })
        check_box_fix = true;

    }
    const letIssue = () => {
            console.log(123);
    }
    const getGenerateScheme = (page = 1) => {
        const amount = $('#amount').val();
        const scheme_id = '{{$scheme_id}}';
        axios.get('{{ route('get-generate-scheme') }}', {
            params: {
                amount,
                page,
                scheme_id
            }
        }).then(({ data }) => {
            lastPage = data.last_page;
            const orders = data.data;
            const scheme_id = data.data[0].scheme_id;
            $('#scheme_id').append(`模擬方案編號: ${scheme_id}`);
            $('#generate-data tbody').empty();
            orders.forEach((order, key) => {
                $('#generate-data tbody').append(`
                    <tr id="${order.id}">
                        <td scope="row">
                            <input type="checkbox" name="mo_id[]" value="${order.id}" group="${key}">
                        </td>
                        <td>
                            <input type="hidden" name="datas[${key}][mo_id]" value="${order.mo_id}">${order.mo_id}
                        </td>
                        <td>
                            <input type="hidden" name="datas[${key}][item_id]" value="${order.item_id}">${order.item_id}
                        </td>
                        <td>${order.so_id}</td>
                        <td>
                            <input type="hidden" name="datas[${key}][qty]" value="${order.qty}">${order.qty}
                        </td>
                        <td>
                            <input type="hidden" name="datas[${key}][cu_ush_date]" value="${order.cu_ush_date}">${order.cu_ush_date}
                        </td>
                        <td>
                            <input type="hidden" name="datas[${key}][resource_id]" value="${order.resource_id}">${order.resource_id}
                        </td>
                        <td>
                            <input type="hidden" name="datas[${key}][aps_id]" value="${order.aps_id}">${order.aps_id}
                        </td>
                        <td>${order.scheme_start}</td>
                        <td>${order.scheme_end}</td>
                        <td>${order.scheme_recommend_lastest_start}</td>
                        <td>${order.scheme_recommend_lastest_end}</td>
                        <td>${order.scheme_recommend_early_start}</td>
                        <td>${order.scheme_recommend_early_end}</td>
                    </tr>
                `)
            });

            $('#pagination-demo').twbsPagination({
                totalPages: lastPage,
                visiblePages: 5,
                first:'頁首',
                last:'頁尾',
                prev:'<',
                next:'>',
                initiateStartPageClick: false,
                onPageClick: function (event, page) {
                    getGenerateScheme(page)
                }
            });
        });
    }
    getGenerateScheme();
</script>
@endsection
