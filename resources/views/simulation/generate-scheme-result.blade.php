@extends('layouts.myapp')

@section('css')
<style>
    .space-item {
        margin-left: 10px;
    }
    .breadcrumb-custom {
        background-color: #3D404C;
        width: 99%;
        margin:0px auto;
        padding: 15px 15px;
        margin-bottom: 20px;
        list-style: none;
        border-radius: 4px;
        color: #fff;
        margin-left:-10px;
    }
    .total-data {
        width: 98%;
        margin:0px auto;
    }
    .table-pos {
        margin: 0px auto;
        width: 98%;
    }
    .thead-color {
        background-color: #E85726;
        color: #fff;
        height: 10px;
    }
    .btn-secondary {
        color: #fff;
        background-color: #6c757d;
        border-color: #6c757d;
    }
    hr {
        border-top: 1px solid #ccc;
    }

    .panel-default {
        border-color: #000000;
    }
    .panel-default > .panel-heading {
        color: #fff;
        background-color: #000000;
        border-color: #000000;
    }
</style>
@endsection

@section('content')
<div id="page-wrapper">
    <div class="container-fluid">
        <h2>初始訂單明細</h2>
        <ol class="breadcrumb">
            <img src="{{ asset('img/u12.png') }}">
            <span class="space-item">排程模擬</span>
            <span class="space-item">></span>
            <span class="space-item">排程來源載入<span>
            <span class="space-item">></span>
            <span class="space-item">初始訂單明細<span>
            <span class="space-item">></span>
            <span class="space-item">初始模擬製令明細<span>
            <span class="space-item">></span>
            <span class="space-item">確認模擬方案<span>
        </ol>
        <div class="breadcrumb-custom">
            <span>資料列表</span>
        </div>
        <div >
            <h3 id="scheme_id" style="margin-left:10px;width:50%"> </h3>
            <span>
                <div align="right" style="">
                    <label style="font-size:18px">工藝路線</label>
                    <select style="margin-right:50px;width:100px"><option>---</option></select>
                </div>
                <div align="right" style="margin-right:40px;">
                    <button style="font-size:18px" class="btn btn-light">一鍵凍結</button>
                    <button style="font-size:18px" class="btn btn-light">取消凍結</button>
                    <button style="font-size:18px" class="btn btn-light">重新凍結</button>
                    <button style="font-size:18px" class="btn btn-light" 
                    onclick="goToSchemeLoadForm()">模擬方案分析結果</button>
                </div>
            </span>
        </div>
        </div>
        <hr>
        <div style="margin-top:15px;">
            {{-- <form action="{{route('confirm-scheme-result')}}" method="GET"> --}}
            <table class="table table-striped table-pos" id="generate-data">
                <thead class="thead-color">
                    <tr>
                        <th scope="col"><input type="checkbox" id="check_all" name="check_all" onclick="checkAll(this)"></th>
                        <th scope="col">製令單號</th>
                        <th scope="col">母件</th>
                        <th scope="col">來源訂單號</th>
                        <th scope="col">數量</th>
                        <th scope="col">預計出貨日</th>
                        <th scope="col">預設資源中心</th>
                        <th scope="col">APS製程碼</th>
                        <th scope="col">計畫開始</th>
                        <th scope="col">計畫結束</th>
                        <th scope="col">建議最遲開始</th>
                        <th scope="col">建議最遲完工</th>
                        <th scope="col">建議最早開始</th>
                        <th scope="col">建議最早完工</th>
                    </tr>
                </thead>
                    <tbody>
                    </tbody>
            </table>

        </div>
            <form action="{{route('scheme-load-form')}}" method="POST" id="scheme-form">
            @csrf
        <div id="data_temp"></div>
                </form>
        <div style="text-align:right">
            <span style="display: inline-block; margin-top: 27px;">
                    <span>每頁顯示筆數</span>
                    <select id="amount" onchange="getGenerateScheme();$('#pagination-demo').twbsPagination('destroy');">
                        <option value="5" selected>5</option>
                        <option value="15">15</option>
                        <option value="20">20</option>
                    </select>
            </span>
            <ul id="pagination-demo" class="pagination-sm" style="vertical-align: top;"></ul>
        </div>
        <hr>
        <div style="text-align:center">
            <button type="button" id="sendBtnC" class="btn btn-success btn-lg" style="width:45%" data-toggle="modal" data-target="#confirm1">確認方案</button>
            <a type="button" id="sendBtnI" class="btn btn-success btn-lg" style="width:45%" data-toggle="modal" data-target="#confirm3" />發佈</a>
            <a class="btn btn-secondary btn-lg" href="javascript:history.back()" style="width:45%">返回</a>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-sm" id="confirm1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">

        <h3  class="text-center" style="margin:50px">是否確認方案?</h3>
        <span class ="border border-secondary">
        <button data-toggle="modal" data-target="#confirm2" class="btn btn-light" style="width:49%;font-size:20px" data-dismiss="modal" >是</button></span>
        <button  class="btn btn-light" data-dismiss="modal" style="width:49%;font-size:20px">否</button>

    </div>
  </div>
</div>
<div class="modal fade bd-example-modal-sm" id="confirm3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">

        <h3  class="text-center" style="margin:50px">確認發佈方案?</h3>
        <span class ="border border-secondary">
        <button data-toggle="modal" data-target="#confirm2" class="btn btn-light" style="width:49%;font-size:20px" data-dismiss="modal" onclick="confirm()">確定</button></span>
        <button  class="btn btn-light" data-dismiss="modal" style="width:49%;font-size:20px">取消</button>

    </div>
  </div>
</div>
<div class="modal fade bd-example-modal-sm" id="confirm2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
        <h3  class="text-center" style="margin:50px" id="success_text">方案確認成功</h3>
        <button class="btn btn-light" style="width:99%;font-size:20px" onclick="confirm()" data-dismiss="modal">確定</button>
  </div>
</div>
{{-- </form> --}}
<script>
    let lastPage;
    $('#sendBtnI').hide();
    let issue = false;
    let check_box_fix = false;
    let  amount = 0 ;
    // const clearcheckbox = () => {   
    //   $checkboxs =  $("input:checked").length ? $("input:checked") : [];
    //   $checkboxs.forEach(
    //     e => e.prop("checked",false)
    //   );
     

    // }
    const goToSchemeLoadForm = () => {
        $('#scheme-form').submit();
    }
    const lockForm = () => {

        const checkno = $('input[name="mo_id[]"]');
        const confirmmo = checkno.map(function(){
            let key = this.value ;
            if(this.checked){
                $(`input[name="mo_id[]"]`).filter(`input[value=${key}]`).hide();
                return Number(this.parentNode.parentNode.id);
            }
            else $(`#${key}`).hide();
        })
        return confirmmo;
    }
    const confirm = () => {
        $('#check_all').hide();
        const e = $(':checkbox');
        if(e.filter(function(){
            return this.checked == true
        }).length) $('#form').submit();
        else {
            alert('請至少選擇一個項目');
            return ;
            }

        let confirmmo =lockForm();
        console.log(confirmmo);
        if(!issue) {
          $('#sendBtnI').show();
          $('#sendBtnC').hide();
          $('#success_text').text('方案發佈成功');
          issue = true;
        } else  letIssue(confirmmo);
        console.log(confirmmo.toArray().toString());
        axios.post('{{route("confirm-mo")}}',{
            confirmmo : confirmmo.toArray().toString()
        })
        check_box_fix = true;

    }
    const letIssue = (issuemo) => {
        axios.post('{{route("issue-mo")}}',{
            issuemo : issuemo.toArray().toString()
        })
    }
    //全選框事件處理
    const checkAll = (I) => {
      const e = $(':checkbox').filter('input[name^="mo_id"]');
      for (var i = e.length - 1; i >= 0; i--) {
        if(I.checked == true) e[i].checked = true ;
        else e[i].checked = false ;
        checkGroup(e[i],e[i].value);
      }

    }
    //單選框事件處理
    const checkGroup = (item,group) => {

      let e = $(`input[group='${group}']`);
      if(e[0].checked == true)
        $('#data_temp').append(`<div id="arr${group}"> </div>`);
      else $(`#data_temp > #arr${group}`).remove();
      for (var i=1;i<e.length;i++){
        e[i].checked = item.checked;
        $(`#data_temp > #arr${group}`).append($(e[i]).clone(true));
      }

    }
    //渲染checkbox 

    const renderCheck =  async () => {
        let temp = $("#data_temp > div");
        temp = temp.map(function(){
            return this.id;
        });
        temp = temp.toArray() ;
            for ( let i = 0;   i < temp.length ;i++){
                $(`#${temp[i]}`).prop('checked',true);
            }   
        for ( let i = 0;   i < temp.length ;i++){

            if(check_box_fix){ 
                $(`#${temp[i]}`).hide();
                $("input[name='mo_id[]").each(function(){
                    if(this.checked == false)
                        this.parentNode.parentNode.hidden = true;
                })
            }
        }
        $("#check_all").prop('checked',false);

    }
    // const recoveryBox = (boxes) =>{
    //     return new Promise(resolve => {
    //         console.log(boxes);
    //         for ( let i = 0;   i < boxes.length ;i++){
    //             $(`#${boxes[i]}`).prop('checked',true);
    //         }
    //         resolve();
    //     });
    // }
    // const filterItem = (boxes,check) =>{
    //     return new Promise(resolve =>{
    //         for ( let i = 0;   i < amount ;i++){

    //             if(check){ 
    //                 $(`#${boxes[i]}`).hide();
    //                 $(`#${boxes[i]}`).parent().parent().show();
    //                 $(":checkbox").each(function(){
    //                     if(this.checked == false)
    //                         this.parentNode.parentNode.hidden =true;
    //                 })
    //             }
    //         }
    //     })
    // }
    const scheme_id = '{{$scheme_id}}';
    $('#scheme_id').append(`模擬方案編號: ${scheme_id} <span style="margin-left:20px">標準初始模擬(已模擬)</span>`);
    const getGenerateScheme = (page = 1) => {
        amount = $('#amount').val();
        axios.get('{{ route('get-generate-scheme') }}', {
            params: {
                amount,
                page,
                scheme_id
            }
        }).then(({ data }) => {
            lastPage = data.last_page;
            const orders = data.data;
            const scheme_id = data.data[0].scheme_id;
            $('#generate-data tbody').empty();
            orders.forEach((order, key) => {
                key = (data.current_page -1)*data.per_page +key;
                $('#generate-data tbody').append(`
                    <tr id="${order.id}">
                        <td scope="row">
                            <input type="checkbox" name="mo_id[]" onclick="checkGroup(this,${key})" group='${key}' value="${key}" id="arr${key}" />
                        </td>
                        <td>
                            <input type="checkbox" name="datas[${key}][mo_id]" value="${order.mo_id}" group="${key}" hidden>${order.mo_id}
                        </td>
                        <td>
                            <input type="checkbox" name="datas[${key}][item_id]" value="${order.item_id}" group="${key}" hidden>${order.item_id}
                        </td>
                        <td>
                            <input type="checkbox" name="datas[${key}][so_id]" value="${order.so_id}" group="${key}" hidden>${order.so_id}
                        </td>
                        <td>
                            <input type="checkbox" name="datas[${key}][qty]" value="${order.qty}" group="${key}" hidden>${order.qty}
                        </td>
                        <td>
                            <input type="checkbox" name="datas[${key}][cu_ush_date]" value="${order.cu_ush_date}" group="${key}" hidden>${order.cu_ush_date}
                        </td>
                        <td>
                            <input type="checkbox" name="datas[${key}][resource_id]" value="${order.resource_id}" group="${key}" hidden>${order.resource_id}
                        </td>
                        <td>
                            <input type="checkbox" name="datas[${key}][aps_id]" value="${order.aps_id}" group="${key}" hidden>${order.aps_id}
                        </td>
                        <td>
                            <input type="checkbox" name="datas[${key}][scheme_start]" value="${order.scheme_start}" group="${key}" hidden>${order.scheme_start}
                        </td>
                        <td>
                            <input type="checkbox" name="datas[${key}][scheme_end]" value="${order.scheme_end}" group="${key}" hidden>${order.scheme_end}
                        </td>
                        <td>
                            <input type="checkbox" name="datas[${key}][scheme_recommend_lastest_start]" value="${order.scheme_recommend_lastest_start}" group="${key}" hidden>${order.scheme_recommend_lastest_start}
                        </td>
                        <td>
                            <input type="checkbox" name="datas[${key}][scheme_recommend_lastest_end]" value="${order.scheme_recommend_lastest_end}" group="${key}" hidden>${order.scheme_recommend_lastest_end}
                        </td>
                        <td>
                            <input type="checkbox" name="datas[${key}][scheme_recommend_early_start]" value="${order.scheme_recommend_early_start}" group="${key}" hidden>${order.scheme_recommend_early_start}
                        </td>
                        <td>
                            <input type="checkbox" name="datas[${key}][scheme_recommend_early_end]" value="${order.scheme_recommend_early_end}" group="${key}" hidden>${order.scheme_recommend_early_end}
                        </td>
                    </tr>
                `)
            })
            $('#generate-data tbody').append(`<input type="hidden" name="scheme_id" value="${scheme_id}" />`);
            $('#pagination-demo').twbsPagination({
                totalPages: lastPage,
                visiblePages: 5,
                first:'頁首',
                last:'頁尾',
                prev:'<',
                next:'>',
                initiateStartPageClick: false,
                onPageClick: function (event, page) {
                    getGenerateScheme(page)
                }
            });
        }).then(()=>{
                renderCheck()
            });
    }
    getGenerateScheme();
    // setTimeout(2000,clearcheckbox);

</script>
@endsection
