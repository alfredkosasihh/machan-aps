@extends('layouts.myapp')

@section('css')
<style>
    .space-item {
        margin-left: 10px;
    }
    .panel-default {
        border-color: #000000;
    }
    .panel-default > .panel-heading {
        color: #fff;
        background-color: #000000;
        border-color: #000000;
    }
    .form-horizontal .control-label {
        text-align: center;
    }
    hr {
        border-top: 1px solid #ccc;
    }
    .btn-secondary {
        color: #fff;
        background-color: #6c757d;
        border-color: #6c757d;
    }
    .btn-secondary:hover {
        color: #fff;
        background-color: #5a6268;
        border-color: #545b62;
    }
    .btn.focus, .btn:focus, .btn:hover {
        color: #fff;
    }
    .inpsize {
        zoom:1.5;
    }
    .table-pos {
        margin: 0px auto;
        width: 98%;
    }
    .thead-color {
        background-color: #E85726;
        color: #fff;
        height: 10px;
    }
    .total-data {
        width: 98%;
        margin:0px auto;
    }
</style>
@endsection

@section('content')
<div id="page-wrapper">
    <div class="container-fluid">
        <h2>查詢結果</h2>
        <ol class="breadcrumb">
            <img src="{{ asset('img/u12.png') }}">
            <span class="space-item">排程模擬</span>
            <span class="space-item">></span>
            <span class="space-item">模擬方案查詢<span>
            <span class="space-item">></span>
            <span class="space-item">查詢結果<span>
        </ol>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">載入條件</div>
                    <div class="panel-body">
                        <table class="table table-striped table-pos" id="generate-data">
                            <thead class="thead-color">
                                <tr>
                                    <th scope="col">序號</th>
                                    <th scope="col">排程方案編號</th>
                                    <th scope="col">排單模擬計畫</th>
                                    <th scope="col">排程狀態</th>
                                    <th scope="col">最早訂單日期</th>
                                    <th scope="col">最遲訂單日期</th>
                                    <th scope="col"> 排程人</th>
                                    <th scope="col">操作</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($datas as $key => $data)
                                <tr>
                                    <th scope="row">{{$key+1}}</th>
                                    <td scope="row">{{$data->scheme_id}}</td>
                                    <td scope="row">1</td>
                                    <td scope="row">{{$status}}</td>
                                    <td scope="row">1</td>
                                    <td scope="row">1</td>
                                    <td scope="row">{{$data->maker}}</td>
                                    <td>
                                        <a href="search-scheme/{{$data->scheme_id}}/result" class="btn btn-success">查看</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <hr>
                        <div style="text-align:center">
                            <a class="btn btn-secondary btn-lg" href="search-scheme" style="width:45%">返回</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
