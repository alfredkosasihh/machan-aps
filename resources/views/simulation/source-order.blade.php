@extends('layouts.myapp')

@section('css')
<style>
    .space-item {
        margin-left: 10px;
    }
    .panel-default {
        border-color: #000000;
    }
    .panel-default > .panel-heading {
        color: #fff;
        background-color: #000000;
        border-color: #000000;
    }
    .form-horizontal .control-label {
        text-align: center;
    }
    hr {
        border-top: 1px solid #ccc;
    }
    .btn-secondary {
        color: #fff;
        background-color: #6c757d;
        border-color: #6c757d;
    }
    .btn-secondary:hover {
        color: #fff;
        background-color: #5a6268;
        border-color: #545b62;
    }
    .btn.focus, .btn:focus, .btn:hover {
        color: #fff;
    }
    .inpsize {
        zoom:1.5;
    }
</style>
@endsection

@section('content')
<div id="page-wrapper">
    <div class="container-fluid">
        <h2>排程來源載入</h2>
        <ol class="breadcrumb">
            <img src="{{ asset('img/u12.png') }}">
            <span class="space-item">排程模擬</span>
            <span class="space-item">></span>
            <span class="space-item">排程來源載入<span>
        </ol>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">載入條件</div>
                    <div class="panel-body">
                        <form class="form-horizontal" action="{{ route('get-source-order') }}" method="GET">
                                            <input type="hidden" name="added" value="0">
                            <div class="form-group">
                                <label class="col-md-2 control-label">組織</label>
                                <div class="col-md-10" id="org">
                                    <select class="form-control" id="sel1" name="org_id" required>
                                        <option disabled selected value="">--- 請選擇廠別 ---</option>
                                    </select>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-2 control-label">結關日期</label>
                                <div class="col-md-4">
                                    <input class="form-control" type="date" id="con_date_start" name="container_date_start">
                                </div>
                                <div class="col-md-2" style="text-align:center;">
                                    <label style="margin:7px;"> ~ </label>
                                </div>
                                <div class="col-md-4">
                                    <input class="form-control" type="date" id="con_date_end" name="container_date_end">
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-2 control-label">單據日期</label>
                                <div class="col-md-4">
                                    <input class="form-control" type="date" id="bill_date_start" name="bill_date_start" required>
                                </div>
                                <div class="col-md-2" style="text-align:center;">
                                    <label style="margin:7px;"> ~ </label>
                                </div>
                                <div class="col-md-4">
                                    <input class="form-control" type="date" id="bill_date_end" name="bill_date_end" required>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-2 control-label">訂單單號</label>
                                <div class="col-md-10">
                                    <input class="form-control" id="so_id" name="so_id">
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-2 control-label">客戶名稱</label>
                                <div class="col-md-10">
                                    <input class="form-control" id="customer_name" name="customer_name">
                                </div>
                                <div id="session-data" hidden>
                                    {{ session('comResults') ?? '' }}
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-2 control-label">訂單狀態</label>
                                <div class="col-md-10" style="font-size:12pt;">
                                    <input class="inpsize" type="checkbox">&ensp;<label>未生效&emsp;</label>
                                    <input class="inpsize" type="checkbox" value="0" name="status[]"><label>已生效&emsp;</label>
                                    <input class="inpsize" type="checkbox" value="1" name="status[]"><label>已模擬&emsp;</label>
                                    <input class="inpsize" type="checkbox" value="2" name="status[]"><label>已確認&emsp;</label>
                                    <input class="inpsize" type="checkbox"><label>已排單&emsp;</label>
                                    <input class="inpsize" type="checkbox"><label>已收回未排單&emsp;</label>
                                    <input class="inpsize" type="checkbox"><label>生產中&emsp;</label>
                                    <input class="inpsize" type="checkbox"><label>已出貨</label>
                                </div>
                            </div>
                            <hr>
                            <div style="text-align:center">
                                <button type="submit" id="sendBtn" class="btn btn-success btn-lg" style="width:45%" onclick="letNoFilter()">載入資料</button>
                                <button type="reset" onclick="resetOption()" class="btn btn-secondary btn-lg" style="width:45%">清除資料</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    const letNoFilter = () => {
        if(!$('input[name="status[]"]').filter(function(){
            return this.checked == true;
        }).length) {
            $('input[name="status[]"]').each(function(){
                this.checked = true;
            })
        }
    }
    const getOrganization = async () => {
        await axios.get('{{ route('getorganization') }}')
            .then(({ data }) => {
                data.forEach(data => {
                    $('#sel1').append(`
                        <option value="${data.factory_id}">${data.name}</option>
                    `);
                });
            });
            if ($('#session-data').text().trim() != '') {
                loadSession();
            }
    }
    getOrganization();

    const loadSession = async () => {
        const saleOrder = JSON.parse('{{ session('comResults') }}'.replace(/&quot;/g,'"'));
        const orgId = '{{ session('org_id') }}';
        let date = new Date();
        let day = date.getDate();
        let month = date.getMonth() + 1;
        let year = date.getFullYear();
        if (month < 10) month = "0" + month;
        if (day < 10) day = "0" + day;
        let today = year + "-" + month + "-" + day;
        let minusHalfMonth = date.setMonth(month - 7);
        let halfDate = new Date(minusHalfMonth);
        let halfDay = halfDate.getDate();
        let halfMonth = halfDate.getMonth() + 1;
        let halfYear = halfDate.getFullYear();
        if (halfMonth < 10) halfMonth = "0" + halfMonth;
        if (halfDay < 10) halfDay = "0" + halfDay;
        let halfToday = halfYear + "-" + halfMonth + "-" + halfDay;
        $('#sel1').val(orgId);
        $('#bill_date_start').val(halfToday);
        $('#bill_date_end').val(today);
        saleOrder.forEach((element, key) => {
            if (key < saleOrder.length - 1) {
                $("#so_id").val(function() {
                    return this.value + element + ',';
                });
            } else {
                $("#so_id").val(function() {
                    return this.value + element;
                });
            }
        });
    }
</script>
@endsection
