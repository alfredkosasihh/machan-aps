@extends('layouts.myapp')

@section('css')
<style>
    .space-item {
        margin-left: 10px;
    }
    .breadcrumb-custom {
        background-color: #3D404C;
        width: 99%;
        margin:0px auto;
        padding: 15px 15px;
        margin-bottom: 20px;
        list-style: none;
        border-radius: 4px;
        color: #fff;
    }
    .total-data {
        width: 98%;
        margin:0px auto;
    }
    .table-pos {
        margin: 0px auto;
        width: 98%;
    }
    .thead-color {
        background-color: #E85726;
        color: #fff;
        height: 10px;
    }
</style>
@endsection

@section('content')
<div id="page-wrapper">
    <div class="container-fluid">
        <h2>產生進度表排程</h2>
        <ol class="breadcrumb">
            <img src="{{ asset('img/u12.png') }}">
            <span class="space-item">資料載入</span>
            <span class="space-item">></span>
            <span class="space-item">產生生產進度表<span>
        </ol>
        <div class="breadcrumb-custom">
            <span>資料列表</span>

        </div>
        <div class="total-data">
            載入筆數 |
            <span id="data-num"></span>
        </div>
        <br>
        <div style="float: left">
            <div class="col-md-10">
                <select placeholder="客戶名稱" id="client_name" name="client" class="form-control" type="text" required>
                    <option disabled selected value="">搜尋客戶名稱</option>
                </select>
                <select placeholder="公益路線" id="techroute" name="client1" class="form-control" type="text" required>
                    <option disabled selected value="">搜尋公藝路線</option>
                </select>
                <button type="button" class="btn btn-success" onclick="searchManufactureData(); $('#pagination-demo-search').twbsPagination('destroy');">搜尋</button>
                <button type="button" class="btn btn-dark" onclick="clears()">清空</button>
            </div>
        </div>
        <div style="float:right; margin:0 10px 5px 0" >
            <button onclick="errorMo()" id="error_check" class="btn btn-danger">異常製令</button>
        </div>
        <br>
        <div class="modal fade" id="err-table" tabindex="-1" role="dialog" aria-labelledby="err-table" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="err-table">【異常製令】</h5>
                    </div>
                    <div class="modal-body"></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal" >確定</button>
                    </div>
                </div>
            </div>
        </div>
        
        <div style="margin-top:15px;">
            <table class="table table-striped table-pos" id="saleorder-data">
                <thead class="thead-color">
                    <tr>
                        <th scope="col">序</th>
                        <th scope="col">客戶名稱</th>
                        <th scope="col">來源訂單號</th>
                        <th scope="col">製令單號</th>
                        <th scope="col">產品代碼</th>
                        <th scope="col">產品名稱</th>
                        <th scope="col">工藝路線</th>
                        <th scope="col">數量</th>
                        <th scope="col">上線日期</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div style="text-align:right" id='init_data'>
            <span style="display: inline-block; margin-top: 27px;">
                    <span>每頁顯示筆數</span>
                    <select id="amount" onchange="getManufactureData();$('#pagination-demo').twbsPagination('destroy');">
                        <option value="10" selected>10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                    </select>
            </span>
            <ul id="pagination-demo" class="pagination-sm" style="vertical-align: top;"></ul>
        </div>
        <div style="text-align:right" id='search_data'>
            <span style="display: inline-block; margin-top: 27px;">
                    <span>每頁顯示筆數</span>
                    <select id="amount_search" onchange="searchManufactureData();$('#pagination-demo-search').twbsPagination('destroy');">
                        <option value="10" selected>10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                    </select>
            </span>
            <ul id="pagination-demo-search" class="pagination-sm" style="vertical-align: top;"></ul>
        </div>
    </div>
</div>
<script>
    let lastPage;
    error_button();

    function error_button(){
        var array_check = {!! json_encode($errorMo) !!};
        var array_check_len = Object.keys(array_check).length;
        if(array_check_len == 0){
            $('#error_check').css('display','none');
        }
    }

    function clears(){
        $('#client_name').val('')
        $('#techroute').val('')
        getManufactureData()
    }

    function loaddata(){
        axios.get('{{ route('list-manufacture-data') }}')
        .then(function (response) {
            client_data = response.data.client;
            client_keys = Object.keys(client_data);
            techroute = response.data.techroute;
            techroute_keys = Object.keys(techroute);
            techroute_id = response.data.techroute_id;
            // techroute_id_keys = Object.keys(techroute_id);
            for(var i=0; i<client_keys.length; i++){
                index = client_keys[i]
                $('#client_name').append(`
                    <option id="clear" value="${client_data[index]}">${client_data[index]}</option>
                `)
            }
            for(var i=0; i<techroute_keys.length; i++){
                index = techroute_keys[i]
                $('#techroute').append(`
                    <option id="clear" value="${techroute_id[index]}">${techroute[index]}</option>
                `)
            }
        })

    }

    const getManufactureData = (page = 1) => {
        $('#init_data').css('display','block');
        $('#search_data').css('display','none');
        const amount = $('#amount').val();
        axios.get('{{ route('manufacture-data') }}', {
            params: {
                amount,
                page,
            }
        }).then(({ data }) => {
            lastPage = data.last_page;
            const orders = data.data;
            $('#data-num').text(`共 ${data.total} 筆`);
            $('#saleorder-data tbody').empty();
            orders.forEach((order, key) => {
                $('#saleorder-data tbody').append(`
                    <tr>
                        <th scope="row">${key + 1 + (page - 1) * amount}</th>
                        <td>${order.customer}</td>
                        <td>${order.so_id}</td>
                        <td>${order.mo_id}</td>
                        <td>${order.item_id}</td>
                        <td>${order.item_name}</td>
                        <td>${order.related_tech_route.tech_routing_name}</td>
                        <td>${order.qty}</td>
                        <td>${order.complete_date}</td>
                    </tr>
                `)
            })
            $('#pagination-demo').twbsPagination({
                totalPages: lastPage,
                visiblePages: 5,
                first:'頁首',
                last:'頁尾',
                prev:'<',
                next:'>',
                initiateStartPageClick: false,
                onPageClick: function (event, page) {
                    getManufactureData(page)
                }
            });
        });
    }

 
    const searchManufactureData = (page = 1) => {
        $('#init_data').css('display','none');
        $('#search_data').css('display','block');
        $('#ini')
        const amount = $('#amount_search').val();
        axios.get('{{ route('search-manufacture-data') }}', {
            params: {
                client : $('#client_name').val(),
                tech_route : $('#techroute').val(),
                amount,
                page,
            }
        }).then(({ data }) => {
            lastPage = data.last_page;
            const orders = data.data;
            $('#data-num').text(`共 ${data.total} 筆`);
            $('#saleorder-data tbody').empty();
            orders.forEach((order, key) => {
                $('#saleorder-data tbody').append(`
                    <tr>
                        <th scope="row">${key + 1 + (page - 1) * amount}</th>
                        <td>${order.customer}</td>
                        <td>${order.so_id}</td>
                        <td>${order.mo_id}</td>
                        <td>${order.item_id}</td>
                        <td>${order.item_name}</td>
                        <td>${order.related_tech_route.tech_routing_name}</td>
                        <td>${order.qty}</td>
                        <td>${order.complete_date}</td>
                    </tr>
                `)
            })
            $('#pagination-demo-search').twbsPagination({
                totalPages: lastPage,
                visiblePages: 5,
                first:'頁首',
                last:'頁尾',
                prev:'<',
                next:'>',
                initiateStartPageClick: false,
                onPageClick: function (event, page) {
                    searchManufactureData(page)
                }
            });
        });
    }
    function errorMo () {
        var array = {!! json_encode($errorMo) !!};
        var len = Object.keys(array).length;
        $('#err-table').modal('show');
        $(".modal-body").html("");
        $('.modal-body').append(`
            <div class="container-fluid">
                <div id="errMo" class="row">
                </div>
            </div>
        `);
        for (i=0; i < len; i++) {
            $("#errMo").append(`
                <p>${i+1}.${array[i]}</p>
            `)
        }
    }
    getManufactureData();
    loaddata();
</script>
@endsection
