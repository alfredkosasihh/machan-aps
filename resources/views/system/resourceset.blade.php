@extends('layouts.myapp')

@section('css')
<style>
    .space-item {
        margin-left: 10px;
    }
    .breadcrumb-custom {
        background-color: #3D404C;
        width: 99%;
        margin:0px auto;
        padding: 15px 15px;
        margin-bottom: 20px;
        list-style: none;
        border-radius: 4px;
        color: #fff;
    }
    .total-data {
        width: 98%;
        margin:0px auto;
    }
    .table-pos {
        margin: 0px auto;
        width: 98%;
    }
    .thead-color {
        background-color: #E85726;
        color: #fff;
        height: 10px;
    }
</style>
@endsection

@section('content')
<div id="page-wrapper">
    <div class="container-fluid">
        <h2>資源設定</h2>
        <ol class="breadcrumb">
            <img src="{{ asset('img/u12.png') }}">
            <span class="space-item">系統管理</span>
            <span class="space-item">></span>
            <span class="space-item">資源設定<span>
        </ol>
        <div class="breadcrumb-custom">
            <span>資料列表</span>
            <div style="float:right; margin-top:-7px">
                <a href="{{ route('resource.create') }}" class="btn btn-success">新增</a>
                <a href="{{ route('resource-sync') }}" class="btn btn-success">同步更新</a>
            </div>
        </div>
        <div class="total-data">
            載入筆數 |
            <span id="data-num"></span>
        </div>
        <div style="margin-top:15px;">
            <table class="table table-striped table-pos" id="resource-table">
                <thead class="thead-color">
                    <tr>
                        <th scope="col">序</th>
                        <th scope="col">資源代碼</th>
                        <th scope="col">資源名稱</th>
                        <th scope="col">設備編號</th>
                        <th scope="col">製程類別</th>
                        <th scope="col">工作中心代碼</th>
                        <th scope="col">操作</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            <div style="text-align:right">
                <span style="display: inline-block; margin-top: 27px;">
                        <span>每頁顯示筆數</span>
                        <select id="amount" onchange="getResData();$('#pagination-demo').twbsPagination('destroy');">
                            <option value="10" selected>10</option>
                            <option value="25">25</option>
                            <option value="50">50</option>
                        </select>
                </span>
                <ul id="pagination-demo" class="pagination-sm" style="vertical-align: top;"></ul>
            </div>
        </div>
    </div>
</div>
<script>
    function checkyn(){
        var check = confirm("是否要刪除該筆資料");
        if (check) {
            return true;
        } else {
            return false;
        }
    }

    let lastPage;
    const getResData = (page = 1) => {
        const amount = $('#amount').val();
        axios.get('{{ route('resourceset-getResData') }}',{
            params: {
                amount,
                page,
            }
        }).then(({ data }) => {
            lastPage = data.last_page;

            const orders = data.data;

            $('#data-num').text(`共 ${data.total} 筆`);
            $('#resource-table tbody').empty();
            orders.forEach((order, key) => {
                $('#resource-table tbody').append(`
                    <tr>
                        <th scope="row">${key + 1 + (page - 1) * amount}</th>
                        <td>${order.resource_id}</td>
                        <td>${order.resource_name}</td>
                        <td>${order.device_id}</td>
                        <td>${order.workcenter.workcenter_name}</td>
                        <td>${order.workcenter_id}</td>
                        <td>
                            <a href="resource/${order.id}/edit" class="btn btn-primary">編輯</a>
                            <form action="resource/${order.id}" onsubmit="return checkyn()" method="POST" style="display:inline-block">
                                @csrf
                                @method('delete')
                                <button class="btn btn-danger">刪除</button>
                            </form>
                        </td>
                    </tr>
                `)
            });
            $('#pagination-demo').twbsPagination({
                totalPages: lastPage,
                visiblePages: 5,
                first:'頁首',
                last:'頁尾',
                prev:'<',
                next:'>',
                initiateStartPageClick: false,
                onPageClick: function (event, page) {
                    getResData(page)
                }
            });
        });
    }
    getResData();
</script>

@endsection
