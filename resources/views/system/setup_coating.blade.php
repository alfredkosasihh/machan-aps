@extends('layouts.myapp')

@section('css')
<style>
    .space-item {
        margin-left: 10px;
    }
    .breadcrumb-custom {
        background-color: #3D404C;
        width: 99%;
        margin:0px auto;
        padding: 15px 15px;
        margin-bottom: 20px;
        list-style: none;
        border-radius: 4px;
        color: #fff;
    }
    .total-data {
        width: 98%;
        margin:0px auto;
        /* padding: 0px 10px; */
    }
    .table-pos {
        margin: 0px auto;
        width: 98%;
    }
    .thead-color {
        background-color: #E85726;
        color: #fff;
        height: 10px;
    }
    form {
        display:inline;
    }
</style>
@endsection

@section('content')
<div id="page-wrapper">
    <div class="container-fluid">
        <h2>吊勾參數設定</h2>
        <ol class="breadcrumb">
            <img src="{{ asset('img/u12.png') }}">
            <span class="space-item">系統管理</span>
            <span class="space-item">></span>
            <span class="space-item">吊勾參數設定<span>
        </ol>
        <div class="breadcrumb-custom" >
            <span>資料列表</span>

            <div style="float:right; margin-top:-7px">
                <form action="{{route('sync')}}"><button class="btn btn-success" method="GET">同步更新</button></form>
                        <button class="btn btn-success" id='new' data-toggle="modal" data-target="#add" style="display:inline">新增</button>
            </div> 
        </div>
        <div class="total-data" id='amount'></div>
        <div style="margin-top:15px;">
            <table class="table table-striped table-pos">
                <thead class="thead-color">
                    <tr>
                        <th scope="col"> 序 </th>
                        <th scope="col">對應子代碼</th>
                        <th scope="col">上料區</th>
                        <th scope="col">圓蓋</th>
                        <th scope="col">格數(掛件總母勾數)</th>
                        <th scope="col">一勾單位量</th>                        
                        <th scope="col">輸送帶刻度</th>
                        <th scope="col">操作</th>
                    </tr>
                </thead> 
                    <tbody id='table_body'>
                    </tbody>

            </table>
        </div>
            <div style="text-align:right">
                <span style="display: inline-block; margin-top: 27px;">
                        <span>每頁顯示筆數</span>
                        <select id="page_amount" onchange="getResData();$('#pagination-demo').twbsPagination('destroy');">
                            <option value="10" selected>10</option>
                            <option value="25">25</option>
                            <option value="50">50</option>
                        </select>
                </span>
                <ul id="pagination-demo" class="pagination-sm" style="vertical-align: top;"></ul>
            </div>
    </div>
</div>
            <form class="form-horizontal" method="POST" action="{{route('coating.store')}}" id="add_form">
                @csrf
                <div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-xl" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h1 class="modal-title" id="exampleModalLabel" align="center">吊勾參數新增</h1>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">

      <div class="form-group">
                                <label class="col-md-4 control-label">對應子代碼</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="cu_pc_material_id" id=""  required>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-4 control-label">上料區</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="cu_p_zone" id="" required>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-4 control-label">圓蓋</label>
                                <div class="col-md-6">
                                    <select class="form-control" form='add_form' name="cu_is_cover" required>
                                            <option value={{true}}>有</option>
                                            <option value={{false}}>無</option>
                                    </select>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-4 control-label">輸送帶刻度</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="cu_rpm" id="" required>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-4 control-label">格數(掛件總母勾數)</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="cu_p_cell_hook_num" id=""  required>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-4 control-label">一勾單位量</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="cu_a_hookm_num" id=""  required>
                                </div>
                            </div>


      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" id='import'>store</button>
        <input type="reset" class = "btn" value="reset" />
    </div>
  </div>
</div>
</div>
</form>


            <form class="form-horizontal" method="POST" action="" id="update_form">
                @csrf
                @method('PUT')
                <div class="modal fade" id="update" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-xl" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h1 class="modal-title" id="exampleModalLabel" align="center">吊勾參數編輯</h1>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">

                            <div class="form-group">
                                <label class="col-md-4 control-label">對應子代碼</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="cu_pc_material_id" id="cu_pcmaterialid" required>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-4 control-label">上料區</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="cu_p_zone" id="cu_pzone" required>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-4 control-label">圓蓋</label>
                                <div class="col-md-6">
                                    <select class="form-control" form='update_form' name="cu_is_cover" required>
                                            <option value={{true}}>有</option>
                                            <option value={{false}}>無</option>
                                    </select>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-4 control-label">輸送帶刻度</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="cu_rpm" id="cu_rpm" required>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-4 control-label">格數(掛件總母勾數)</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="cu_p_cell_hook_num" id="cu_pcellhooknum" required>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-4 control-label">一勾單位量</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="cu_a_hookm_num" id="cu_ahookmnum"  required>
                                </div>
                            </div>


      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" id='update'>update</button>
        <input type="reset" class = "btn" value="reset" />
      </div>
  </div>
</div>
</div>
</form>



<script>
    const setCoverVal = () => 
    {
        let val = $('#sel').val();
        if(val == '0'){
            val = false;
        }
        else if(val =='1'){
            val = true;
        }
        $('#IsCover').text(123);
    }
    let lastPage;
    const printData = (page = 1) => {
        const amount = $('#page_amount').val();
        axios.get('{{ route('coating-data') }}', {
            params: {
                amount,
                page,
            }
        }).then(({ data }) => {
            lastPage = data.last_page;
            const  items = data.data;
            $('#data-num').text(`共 ${data.total} 筆`);
            $('#table_body').empty();
            items.forEach((item, key) => {
                if (item.cu_is_cover) {
                    item.cu_is_cover = "有"
                }
                else {
                    item.cu_is_cover = "無"
                }
                $('#table_body').append(`
                    <tr id='${item.id}'>
                        <th scope="row">${key + 1 + (page - 1) * amount}</th>
                        <td id='td_cu_pcmaterialid'>${item.cu_pc_material_id}</td>
                        <td id='td_cu_pzone'>${item.cu_p_zone}</td>
                        <td id='td_cu_iscover'>${item.cu_is_cover}</td>
                        <td id='td_cu_pcellhooknum'>${item.cu_p_cell_hook_num}</td>
                        <td id='td_cu_ahookmnum'>${item.cu_a_hookm_num}</td>
                        <td id='td_cu_rpm'>${item.cu_rpm}</td>


                        <td >
                            <button name='${item.id}' class="btn btn-primary" id="edit" onclick="pushData(${item.id})" data-toggle="modal" data-target="#update">編輯</button>
                            <form action="coating/${item.id}" method="POST" style="display:inline-block">
                                @csrf
                                @method('delete') 
                                <button class="btn btn-danger">刪除</button> 
                            </form> 
                        </td>
                    </tr>
                `)
            });

            $('#pagination-demo').twbsPagination({
                totalPages: lastPage,
                visiblePages: 5,
                first:'頁首',
                last:'頁尾',
                prev:'<',
                next:'>',
                initiateStartPageClick: false,
                onPageClick: function (event, page) {
                    printData(page)
                }
            });
        });
    }

    const pushData = (val) => {
        console.log(val);
        $('#cu_pcmaterialid').val($(`#${val} #td_cu_pcmaterialid`).text())
        $('#cu_pzone').val($(`#${val} #td_cu_pzone`).text())
        $('#cu_iscover').val($(`#${val} #td_cu_iscover`).text())
        $('#cu_pcellhooknum').val($(`#${val} #td_cu_pcellhooknum`).text())
        $('#cu_rpm').val($(`#${val} #td_cu_rpm`).text())
        $('#cu_ptotallen').val($(`#${val} #td_cu_ptotallen`).text())
        $('#cu_ahookmnum').val($(`#${val} #td_cu_ahookmnum`).text())


        //動態編輯路由
        $('#update_form').attr('action','coating/'+val)
    }

    printData();
</script>
@endsection
