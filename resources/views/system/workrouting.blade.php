@extends('layouts.myapp')

@section('css')
<style>
    .space-item {
        margin-left: 10px;
    }
    .breadcrumb-custom {
        background-color: #3D404C;
        width: 99%;
        margin:0px auto;
        padding: 15px 15px;
        margin-bottom: 20px;
        list-style: none;
        border-radius: 4px;
        color: #fff;
    }
    .total-data {
        width: 98%;
        margin:0px auto;
        /* padding: 0px 10px; */
    }
    .table-pos {
        margin: 0px auto;
        width: 98%;
    }
    .thead-color {
        background-color: #E85726;
        color: #fff;
        height: 10px;
    }
</style>
@endsection

@section('content')
<div id="page-wrapper">
    <div class="container-fluid">
        <h2>工藝路線設定</h2>
        <ol class="breadcrumb">
            <img src="{{ asset('img/u12.png') }}">
            <span class="space-item">系統管理</span>
            <span class="space-item">></span>
            <span class="space-item">工藝路線設定<span>
        </ol>
        <div class="breadcrumb-custom">
            <span>資料列表</span>
            <div style="float:right; margin-top:-7px">
                <button class="btn btn-success">新增</button>
            </div> 
        </div>
        <div class="total-data">載入筆數 | 共 5 筆</div>
        <div style="margin-top:15px;">
            <table class="table table-striped table-pos">
                <thead class="thead-color">
                    <tr>
                        <th scope="col">序</th>
                        <th scope="col">工藝路線</th>
                        <th scope="col">預設班別</th>
                        <th scope="col">標準TCT(秒)</th>
                        <th scope="col">預設資源中心</th>
                        <th scope="col">操作</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="row">1</th>
                        <td>一群-雷射</td>
                        <td>正常班_標準</td>
                        <td>60</td>
                        <td>一群雷射機</td>
                        <td>
                            <button class="btn btn-primary">編輯</button>
                            &nbsp
                            <button class="btn btn-danger">刪除</button>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">2</th>
                        <td>一群-P2</td>
                        <td>正常班_標準</td>
                        <td>60</td>
                        <td>一群CNC轉塔沖孔機</td>
                        <td>
                            <button class="btn btn-primary">編輯</button>
                            &nbsp
                            <button class="btn btn-danger">刪除</button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection