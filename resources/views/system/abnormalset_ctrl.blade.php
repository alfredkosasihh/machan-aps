@extends('layouts.myapp')

@section('css')
<style>
    .space-item {
        margin-left: 10px;
    }
    .panel-default {
        border-color: #000000;
    }
    .panel-default > .panel-heading {
        color: #fff;
        background-color: #000000;
        border-color: #000000;
    }
    .form-horizontal .control-label {
        text-align: center;
    }
    hr {
        border-top: 1px solid #ccc;
    }
    .btn-secondary {
        color: #fff;
        background-color: #6c757d;
        border-color: #6c757d;
    }
    .btn-secondary:hover {
        color: #fff;
        background-color: #5a6268;
        border-color: #545b62;
    }
    .btn.focus, .btn:focus, .btn:hover {
        color: #fff;
    }
    .inpsize {
        zoom:1.5;
    }

</style>
@endsection

@section('content')
<div id="page-wrapper">
    <div class="container-fluid">
        <h2>異常原因設定</h2>
        <ol class="breadcrumb">
            <img src="{{ asset('img/u12.png') }}">
            <span class="space-item">系統管理</span>
            <span class="space-item"> > 異常原因設定</span>
            <span class="space-item"> > 編輯異常原因<span>
        </ol>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">資料編輯</div>
                    <div class="panel-body">
                        @if($entity)
                            <form class="form-horizontal" action="{{ url('abnormal-fix/'.$id) }}" method="GET">
                        
                        @else
                            <form class="form-horizontal" action="{{ route('abnormal-add') }}" method="GET">

                        
                        @endif

                            @csrf
                            <div class="form-group">
                                <label class="col-md-2 control-label">異常類別</label>
                                <div class="col-md-10" id="org">
                                    <select class="form-control" id="sel1" name="Type" required>
                                        <option>模擬</option>
                                        <option>載入</option>
                                        <option>發放</option>
                                        <option>作業</option>
                                        <option>同步</option>
                                        <option>系統</option>
                                    </select>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-2 control-label">異常原因</label>
                                <div class="col-md-10">
                                    @if ($entity) 
                                        <input class="form-control" id="so_id" name="Exception"
                                                value='{{$entity->Exception}}' required>
                                    
                                    @else 
                                        <input class="form-control" id="so_id" name="Exception"
                                                value='' required>
                                    
                                    @endif
                                    
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-2 control-label">EX_Code</label>
                                <div class="col-md-10">
                                    @if ($entity) 
                                        <input class="form-control" id="so_id" name="EX_Code"
                                                value='{{$entity->EX_Code}}' required>
                                    
                                    @else 
                                        <input class="form-control" id="so_id" name="EX_Code"
                                                value='' required>
                                    
                                    @endif
                                    
                                </div>
                            </div>
                            <hr>

                            <div style="text-align:center">
                                <button  type="submit" id="sendBtn" class="btn btn-success btn-lg" style="width:45%">確定</button>
                                <button type="reset" onclick="resetOption()" class="btn btn-secondary btn-lg" style="width:45%" >清除</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    const resetOption = () =>{
        console.log('clear');
            $('#so_id').attr("value","");

    }
</script>
@endsection