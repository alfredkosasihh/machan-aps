<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>APS</title>

    <!-- Scripts -->
    {{-- <script src="{{ asset('vendor/jquery/jquery.min.js') }}" defer></script> --}}
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}" defer></script>
    <script src="{{ asset('vendor/metisMenu/metisMenu.min.js') }}" defer></script>
    <script src="{{ asset('dist/js/sb-admin-2.js') }}" defer></script>
    <script src="{{ asset('js/axios.min.js')}}"></script> <!-- 19/12/24 use axios js -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    {{-- <script src="https://unpkg.com/axios/dist/axios.min.js"></script> --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.10/lodash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twbs-pagination/1.4.2/jquery.twbsPagination.js"></script>
<script src="https://unpkg.com/react@16/umd/react.production.min.js" crossorigin></script>
<script src="https://unpkg.com/react-dom@16/umd/react-dom.production.min.js" crossorigin></script>
<script src="https://unpkg.com/babel-standalone@6/babel.min.js"></script>
    <!-- Styles -->
    <link href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/metisMenu/metisMenu.min.css') }}" rel="stylesheet">
    <link href="{{ asset('dist/css/sb-admin-2.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">

    @yield('css')
    <style>
   /*側選單樣式*/
        .a-font {
            color: white;
        }
        .sidebar ul li {
            border: 0px;
        }
        .navbar-default {
            border-color: #343641;
        }
        .nav>li>a:focus, .nav>li>a:hover {
            background-color: #343641;
            font-family:Microsoft JhengHei;
            font-size:22px;
            font-weight:700;

        }
        .nav>li>a{
            background-color: #3D404C;
            font-family:Microsoft JhengHei;
            font-size:22px;
            font-weight:700;            
        }
        .nav .open>a, .nav .open>a:focus, .nav .open>a:hover {
            background-color: #343641;
                font-family:Microsoft JhengHei;
        }
        .sidebar ul li a.active {
            background-color: #343641;
        }

    /*頁面標題字樣式*/
        .container-fluid>h2{
            margin-left:5px;
            font-size :28px ;
            font-weight:bold;
        }
        .head{
            margin-left:40px;
            font-size :28px ;
            font-weight:bold;       
        }

    /*字型大小*/
        span,div,th{
            font-family:Microsoft JhengHei;
        }
        .form-group>label{
            font-size:20px !important;
        }
        .breadcrumb>.space-item{
            font-size:16px;
        }
        .title{
            font-size:18px;
        }
        .breadcrumb-custom>span{
            font-size:18px;
        }
        .panel-heading{
            font-size:18px;         
        }

    /*麵包屑位置*/
        .container-fluid>.breadcrumb{
            margin-left:-45px;
            padding-left :30px;
            margin-bottom:20px;
        }
        .breadcrumb{
            margin-left:245px;
            padding-left :30px;
            margin-bottom:0px;
        }

    /*表格位置*/
        .table_position{
                       padding-top: 20px !important;
        }

        .content{
                padding-top: 10px !important;
        }

    /*表格樣式*/
        .table_border{
            border :1px solid;
            border-radius :5px;
        }
        .table_margin{
            margin:3%;
            margin-top:0px;
        }
        th{
            width:27px;
            font-size:20px;
        }
        .back_btn_fixed{
            margin-bottom :10px;
        }
        .success_btn_fixed{
            color :white;
            margin-bottom :10px;
            background-color: #E85524;    

        }
        .floor_fixed{
            width:98%;
        }
    </style>
</head>
<body>
    <div id="wrapper" style="background-color: #3D404C;">
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0; background-color: #3D404C;">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="{{ asset('img/u9.png') }}" width="125px">
                </a>
            </div>

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" style="color: white;">
                        {{ Auth::user()->role->name }}
                        <i class="fa fa-user fa-fw"></i> {{ Auth::user()->name }} <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                            <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                    <i class="fa fa-sign-out fa-fw"></i>Logout
                            </a>

                        </li>
                    </ul>
                </li>
            </ul>

            <div class="navbar-default sidebar" role="navigation" style="background-color: #3D404C;">
                <div id="sidebar-option" class="sidebar-nav navbar-collapse" hidden>
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="{{ route('home') }}" style="color: white;">首頁</a>
                        </li>
                        {{-- <li>
                          <a href="#" style="color: white;">系統管理<span class="fa arrow"></span></a>
                          <ul class="nav nav-second-level">
                              <li>
                                  <a href="#" style="color: white;">班表設定<span class="fa arrow"></span></a>
                                  <ul class="nav nav-third-level">
                                      <li>
                                          <a href="{{ route('year-calendar') }}" style="color: white;">公司行事曆</a>
                                      </li>
                                      <li>
                                          <a href="{{ route('process-calendar') }}" style="color: white;">製程行事曆</a>
                                      </li>
                                      <li>
                                          <a href="#" style="color: white;">班別設定</a>
                                      </li>
                                      <li>
                                          <a href="#" style="color: white;">休息時間設定</a>
                                      </li>
                                  </ul>
                              </li>
                          </ul>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="#" style="color: white;">資料載入<span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <li>
                                            <a href="#" style="color: white;">載入行事曆</a>
                                        </li>
                                        <li>
                                            <a href="#" style="color: white;">載入訂單</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li> --}}
                        <li>
                            <a href="#" style="color: white;">系統設定<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{ route('year-calendar') }}" style="color: white;">公司行事曆</a>
                                </li>
                                <li>
                                    <a href="{{ route('process-calendar') }}" style="color: white;">製程行事曆</a>
                                </li>
                                <li>
                                    <a href="{{ route('work-type.index') }}" style="color: white;">班別設定</a>
                                </li>
                                <li>
                                    <a href="{{ route('rest-time.index') }}" style="color: white;">休息時間設定</a>
                                </li>
                                <li>
                                    <a href="{{ route('tech-routing.index') }}" style="color: white;">工藝路線設定</a>
                                </li>
                                <li>
                                    <a href="{{ route('work-center') }}" style="color: white;">工作中心設定</a>
                                </li>
                                <li>
                                    <a href="{{ route('resource.index') }}" style="color: white;">資源設定</a>
                                </li>
                                <li>
                                    <a href="{{ route('coating.index') }}" style="color: white;">吊勾參數設定</a>
                                </li>
                                <li>
                                    <a href="{{ route('abnormal-set') }}" style="color: white;">異常原因設定</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#" style="color: white;">帳號管理<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{ route('management.index') }}" style="color: white;">人員帳號管理</a>
                                </li>
                                <li>
                                    <a href="{{ route('role-set.index') }}" style="color: white;">角色設定</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#" style="color: white;">資料載入<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{ route('load-schedule') }}" style="color: white;">載入進度表</a>
                                </li>
                                <li>
                                    <a href="{{ route('sale-order') }}" style="color: white;">載入T9訂單</a>
                                </li>
                                <li>
                                    <a href="{{ route('search-manufacture') }}" style="color: white;">搜尋進度表</a>
                                </li>
                                <li>
                                    <a href="{{ route('search-bom') }}" style="color: white;">BOM查詢</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#" style="color: white;">排程模擬<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{ route('source-order') }}" style="color: white;">排程來源載入</a>
                                </li>
                                <li>
                                    <a href="{{ route('search-scheme') }}" style="color: white;">模擬方案查詢</a>
                                </li>
                                <li>
                                    <a href="" style="color: white;">方案發佈</a>
                                </li>
                                <li>
                                    <a href="" style="color: white;">方案回收</a>
                                </li>
                                <li>
                                    <a href="" style="color: white;">刪除方案</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        @yield('content')
    </div>
    @yield('js')
</body>
</html>
