@extends('layouts.myapp')

@section('css')
<style>
    .bg {
        background-image: url("/img/u14.jpg");
        height: 100%;
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
    }
    .title-text {
        text-align: center;
        position: absolute;
        top: 50%;
        left: 55%;
        transform: translate(-50%, -50%);
        border: 4px solid rgb(0, 115, 230, 0.3);
        border-radius: 25px;
        width: 25%;
        color: #0072e3;
        font-family: Geneva, Arial, Helvetica, sans-serif, Microsoft JhengHei;
    }
</style>
@endsection

@section('content')
<div id="page-wrapper" class="bg">
    <div class="title-text">
        <h1 style="font-size:50px">APS</h1>
        <h1 style="font-size:50px">線上排單系統</h1>
    </div>
</div>    
@endsection